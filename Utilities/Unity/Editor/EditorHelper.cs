﻿using UnityEditor;
using UnityEngine;

public class EditorHelper : MonoBehaviour {
    
    //removes prefab from the selected object. shortcut - ctrl + r, or cmd + r
    [MenuItem("TMI/Remove Prefab")]
    private static void RemovePrefab()
    {
        GameObject disconnectingObj = Selection.activeGameObject;
        Selection.activeGameObject = null;
        PrefabUtility.DisconnectPrefabInstance(disconnectingObj);
        Object prefab = PrefabUtility.CreateEmptyPrefab("Assets/dummy.prefab");
        PrefabUtility.ReplacePrefab(disconnectingObj, prefab, ReplacePrefabOptions.ConnectToPrefab);
        PrefabUtility.DisconnectPrefabInstance(disconnectingObj);
        AssetDatabase.DeleteAsset("Assets/dummy.prefab");
        Selection.activeObject = disconnectingObj;
    }

    [MenuItem("TMI/Remove Prefab", true)]
    private static bool CanRemovePrefab()
    {
        return Selection.activeGameObject != null && PrefabUtility.GetPrefabParent(Selection.activeGameObject);
    }

}
