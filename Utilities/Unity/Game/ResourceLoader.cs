﻿using UnityEngine;
using System.Collections;

public static class ResourceLoader {

    //private static ResourceLoader s_ResourceLoader;

    //private ResourceLoader() { }

    //public static ResourceLoader Instance
    //{
    //    get {
    //        return s_ResourceLoader == null ? s_ResourceLoader = new ResourceLoader() : s_ResourceLoader;
    //    }
    //}

    public static Object Load(string path)
    {
        return Resources.Load(path);
    }

    public static T[] LoadAll<T>(string path) where T: Object
    {
        return Resources.LoadAll<T>(path);
    }

    public static T Load<T>(string path) where T: Object
    {
        return Resources.Load<T>(path);
    }
}
