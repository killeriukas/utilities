﻿using UnityEngine;
using System.Collections;

public class CacheMonoBehaviour : MonoBehaviour {

    private Transform m_CacheTransform;

    new public Transform transform
    {
        get {
            if (m_CacheTransform == null)
            {
                m_CacheTransform = base.transform;
            }
            return m_CacheTransform;
        }
    }

}
