﻿using UnityEngine;
using System.Collections;

public class DontDestroyGO : MonoBehaviour
{
    private void Awake()
    {
        GameObject.DontDestroyOnLoad(gameObject);
    }
}
