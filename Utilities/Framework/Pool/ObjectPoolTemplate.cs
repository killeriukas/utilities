﻿using System.Collections.Generic;
using System;
using UnityEngine;

public class ObjectPoolUnity<T, U>  where U : IPoolableObject
{
	private Dictionary<T, U> m_PoolableObjects;

    private Transform m_MainParentTransform;

	public ObjectPoolUnity(List<T> poolableItem, Func<T, U> generatorFunction)
	{
        GameObject pool = new GameObject("ObjectPoolUnity");
        m_MainParentTransform = pool.transform;

		m_PoolableObjects = new Dictionary<T, U>();
		for(int i = 0; i < poolableItem.Count; ++i)
		{
            if (poolableItem[i] != null)
            {
                U frontEndItem = generatorFunction(poolableItem[i]);
			    frontEndItem.Deactivate();
                frontEndItem.GetTransform().SetParent(m_MainParentTransform, false);
			    m_PoolableObjects.Add(poolableItem[i], frontEndItem);
            }
		}
	}

	public U GetPooledItemByKey(T key)
	{
        U foundItem = m_PoolableObjects[key];
        foundItem.Activate();
        return foundItem;
        //U foundItem = default(U);

        //if(m_PoolableObjects.TryGetValue(key, out foundItem))
        //{
        //    foundItem.Activate();
        //}
        //else
        //{
        //    Logger.Log("Can't find item in the pool by key: " + key);
        //}
        //return foundItem;
	}

	public void PutItemToPool(T key)
	{
        U pooledItem = m_PoolableObjects[key];
		pooledItem.Deactivate();
        pooledItem.GetTransform().SetParent(m_MainParentTransform, false);
	}

}