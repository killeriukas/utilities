﻿using UnityEngine;

public interface IPoolableObject
{
	void Activate();
	void Deactivate();
    Transform GetTransform();
}
