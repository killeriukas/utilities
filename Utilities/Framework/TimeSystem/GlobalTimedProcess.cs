﻿using System;

//global time runs even if game is not running
//mostly used for long time intervals and RTS/RPG games
public class GlobalTimedProcess : TimedProcessBase
{
    private readonly int EXPIRATION_SEC_UTC;
    private readonly int DURATION_SEC;

    //public GlobalTimedProcess(int expireTimeSecUTC, Action methodToFire) : base(methodToFire)
    //{
    //    EXPIRATION_SEC_UTC = expireTimeSecUTC;
    //    DURATION_SEC = expireTimeSecUTC - TrustedTime.GetTrustedTimeSecUTC();
    //}

    public GlobalTimedProcess(int durationSec, Action methodToFire) : base(methodToFire)
    {
        EXPIRATION_SEC_UTC = TrustedTime.GetTrustedTimeSecUTC() + durationSec;
        DURATION_SEC = durationSec;
    }

    public GlobalTimedProcess(int startTimeSecUTC, int durationSec, Action methodToFire) : base(methodToFire)
    {
        EXPIRATION_SEC_UTC = startTimeSecUTC + durationSec;
        DURATION_SEC = durationSec;
    }

    public override bool HasFinished(int deltaTimeMS)
    {
        return EXPIRATION_SEC_UTC < TrustedTime.GetTrustedTimeSecUTC();
    }

    public override float GetCompletedPercentage()
    {
        return 1.0f - MathLib.Clamp01((EXPIRATION_SEC_UTC - TrustedTime.GetTrustedTimeSecUTC()) / (float)DURATION_SEC);
    }

    public override int GetExpirationTime()
    {
        return EXPIRATION_SEC_UTC;
    }
}