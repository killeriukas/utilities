﻿using System;
public abstract class TimedProcessBase
{
    private Action m_FireMethodAtFinish;
    public abstract bool HasFinished(int deltaTimeMS);
    public abstract float GetCompletedPercentage();
    public abstract int GetExpirationTime();

    public TimedProcessBase(Action methodToFire)
    {
        m_FireMethodAtFinish = methodToFire;
    }

    public void FireMethodOnce()
    {
        if (m_FireMethodAtFinish != null)
        {
            m_FireMethodAtFinish();
            m_FireMethodAtFinish = null;
        }
    }

    public void Destroy()
    {
        m_FireMethodAtFinish = null;
    }

}