﻿using System.Collections.Generic;
public sealed class TimedProcessesManager
{

    private static TimedProcessesManager s_TimedProcessesManager;
    private Dictionary<uint, TimedProcessBase> m_TimedProcessesDict;
    private List<uint> m_FinishedProcessesList;
    private uint m_LastTimedProcessIndex = 0;

    public static TimedProcessesManager Instance
    {
        get {
            return s_TimedProcessesManager == null ? s_TimedProcessesManager = new TimedProcessesManager() : s_TimedProcessesManager;
        }
        private set { }
    }

    private TimedProcessesManager()
    {
        m_TimedProcessesDict = new Dictionary<uint, TimedProcessBase>();
        m_FinishedProcessesList = new List<uint>();
    }

    //public TimedProcessBase CreateTimedProcess()
    //{
    //    //REQUIRES IMPLEMENTATION
    //    throw new System.NotImplementedException();
    //}

    public uint AddTimedProcess(TimedProcessBase timedProcess)
    {
       uint index = m_LastTimedProcessIndex++;
       m_TimedProcessesDict.Add(index, timedProcess);
       return index;
    }

    public TimedProcessBase GetTimedProcess(uint index)
    {
        TimedProcessBase timedProcess = null;
        if(m_TimedProcessesDict.ContainsKey(index))
        {
            timedProcess = m_TimedProcessesDict[index];
        }
        else
        {
            Logger.Log("Timed process with index " + index + " doesn't exist in the dictionary!", Logger.MessageLevel.MESSAGE_LEVEL_ERROR_LEVEL_0);
        }
        return timedProcess;
    }

    //called every frame
    public void Update(int deltaTimeSec)
    {

        //collect all processes which have already finished
        foreach(var kvp in m_TimedProcessesDict)
        {
            if (kvp.Value.HasFinished(deltaTimeSec))
            {
                m_FinishedProcessesList.Add(kvp.Key);
            }
        }

        //activate all finished processes and remove them from global dictionary, if any
        if (m_FinishedProcessesList.Count > 0)
        {
            for (int i = 0; i < m_FinishedProcessesList.Count; ++i)
            {
                m_TimedProcessesDict[m_FinishedProcessesList[i]].FireMethodOnce();
                m_TimedProcessesDict.Remove(m_FinishedProcessesList[i]);
            }
            m_FinishedProcessesList.Clear();
        }
    }

    public void Destroy()
    {
        if (m_TimedProcessesDict != null)
        {
            foreach (var kvp in m_TimedProcessesDict)
            {
                kvp.Value.Destroy();
            }
            m_TimedProcessesDict.Clear();
            m_TimedProcessesDict = null;
        }
        if (m_FinishedProcessesList != null)
        {
            m_FinishedProcessesList.Clear();
            m_FinishedProcessesList = null;
        }
        s_TimedProcessesManager = null;
    }

}