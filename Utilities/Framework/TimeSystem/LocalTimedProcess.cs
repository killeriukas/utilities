﻿using System;

//this timer runs only while app is running
//mostly used for short intervals
public class LocalTimedProcess : TimedProcessBase
{

    private int m_durationMS;
    private readonly int MAX_DURATION_MSEC;

    public LocalTimedProcess(int durationMSec, Action methodToFire) : base(methodToFire)
    {
        MAX_DURATION_MSEC = m_durationMS = durationMSec;
    }

    public LocalTimedProcess(int maxDurationMSec, int durationLeftMSec, Action methodToFire) : base(methodToFire)
    {
        MAX_DURATION_MSEC = maxDurationMSec;
        m_durationMS = durationLeftMSec;
    }

    public override bool HasFinished(int deltaTimeMS)
    {
        m_durationMS -= deltaTimeMS;
        return m_durationMS < 0;
    }

    public override float GetCompletedPercentage()
    {
        return 1.0f - MathLib.Clamp01(m_durationMS / (float)MAX_DURATION_MSEC);
    }

    public override int GetExpirationTime()
    {
        return m_durationMS;
    }
}