﻿public sealed class TrustedTime
{

    private int m_difference = 0;
    private static TrustedTime s_TrustedTime;
    private readonly System.DateTime START_EPOCH_TIME = new System.DateTime(1970, 1, 1);

    private double GetCurrentTimeSec()
    {
        return (System.DateTime.UtcNow - START_EPOCH_TIME).TotalSeconds;
    }

    public void SetTrustedTimeSec(int trustedTimeSec)
    {
        m_difference = trustedTimeSec - (int)GetCurrentTimeSec();
    }

    private int TrustedTimeSecUTC()
    {
        return (int)GetCurrentTimeSec() + m_difference;
    }

    public static int GetTrustedTimeSecUTC()
    {
        return TrustedTime.Instance.TrustedTimeSecUTC();
    }

    public static TrustedTime Instance
    {
        get {
            return s_TrustedTime == null ? s_TrustedTime = new TrustedTime() : s_TrustedTime;
        }
        private set { }
    }

    public void Destroy()
    {
        s_TrustedTime = null;
    }
}