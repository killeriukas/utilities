﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public sealed class SceneController : Singleton<SceneController>
{
    private enum SceneState
    {
        UNKNOWN = 0,
        LOADING_LOADING_SCREEN,
        UNLOADING_CURRENT_SCENE,
        LOADING_NEXT_SCENE,
        UNLOADING_LOADING_SCREEN,
        CURRENT_SCENE_READY
    }

    private Scene m_CurrentScene;

    public A GetCurrentScene<A>() where A : Scene
    {
        return (A)m_CurrentScene;
    }

    private Scene m_NextScene;
    private LoadingScreen m_LoadingScreen;
    private SceneState m_CurrentSceneState;

    public UIScreenController _UIScreenController { get; private set; }
    public UIPopupController _UIPopupController { get; private set; }

    //public void SetUIPanel (UIPanel uiPanel)
    //{
    //    m_UIPopupController.SetUIPanel(uiPanel);
    //    UIScreenController.SetUIPanel(uiPanel);
    //}

    private SceneController()
    {
        m_CurrentSceneState = SceneState.UNKNOWN;
        _UIScreenController = new UIScreenController();
		_UIPopupController = new UIPopupController();
    }

	public void StartingScene (Scene newScene)
	{
		m_CurrentScene = newScene;
		m_CurrentScene.StartScene();
		m_CurrentSceneState = SceneState.CURRENT_SCENE_READY;
	}

	public void LoadNewScene(Scene newScene, bool loadAsync, LoadingScreen loadingScreen)
    {
        if (m_CurrentSceneState == SceneState.LOADING_LOADING_SCREEN || m_CurrentSceneState == SceneState.LOADING_NEXT_SCENE)
        {
            return;
        }

        m_NextScene = newScene;
        m_CurrentSceneState = SceneState.UNLOADING_CURRENT_SCENE;
        m_NextScene.LoadScene(loadAsync);

        //m_LoadingScreen = loadingScreen;
        //m_LoadingScreen.SetLoadingScreen();


        //newScene.LoadLevel(loadAsync);
    }

    public float GetLoadPercentage()
    {
        return m_NextScene == null ? 0.0f : m_NextScene.GetLoadPercentage();
    }

    public void Update(float deltaTimeSec)
    {
        switch(m_CurrentSceneState)
        {
            case SceneState.UNKNOWN:

                break;
            case SceneState.LOADING_LOADING_SCREEN:
               // m_LoadingScreen.Update(deltaTimeSec);
                //if (m_LoadingScreen.IsReady())
                //{
                //    m_NextScene.LoadLevel(loadAsync);
                //    m_CurrentSceneState = SceneState.SCENE_STATE_LOADING_NEXT_SCENE;
                //    m_LoadingScreen.Destroy();
                //    m_LoadingScreen = null;
                //}



                break;
			case SceneState.UNLOADING_CURRENT_SCENE:
				m_CurrentScene.Dispose();
				m_CurrentSceneState = SceneState.LOADING_NEXT_SCENE;
				break;
            case SceneState.LOADING_NEXT_SCENE:
                m_NextScene.UpdateLoading(deltaTimeSec);
                if (m_NextScene.IsReady())
                {
                    m_CurrentScene = m_NextScene;
                    m_NextScene.StartScene();
                    m_NextScene = null;
                    m_CurrentSceneState = SceneState.CURRENT_SCENE_READY;
                }
                break;

			case SceneState.UNLOADING_LOADING_SCREEN:
                //none as of yet
				break;
            case SceneState.CURRENT_SCENE_READY:
                m_CurrentScene.UpdateScene(deltaTimeSec);
                break;
        }
    }

    public override void Dispose()
    {
        _UIScreenController.Dispose();
        _UIScreenController = null;
        _UIPopupController.Dispose();
        _UIPopupController = null;
    }

}