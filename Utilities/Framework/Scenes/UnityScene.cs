﻿using UnityEngine;

public abstract class UnityScene : Scene {

    private AsyncOperation m_AsyncOp;
    private readonly string m_SceneName;

    public UnityScene(string sceneName)
    {
        m_SceneName = sceneName;
    }
    
    public sealed override bool IsReady()
    {
        return m_AsyncOp == null ? Application.loadedLevelName == m_SceneName : m_AsyncOp.progress > 0.89f;
    }

    public sealed override float GetLoadPercentage()
    {
        return m_AsyncOp == null ? 1.0f : m_AsyncOp.progress;
    }

    public sealed override void LoadScene(bool loadAsync)
    {
        //ASYNC LOADING DOESN'T WORK!!!!
        loadAsync = false;
        if (loadAsync)
        {
            m_AsyncOp = Application.LoadLevelAsync(m_SceneName);
            m_AsyncOp.allowSceneActivation = false;
        }
        else
        {
            Application.LoadLevel(m_SceneName);
        }
    }
   
}
