﻿using System.Collections.Generic;
using System;

public abstract class Scene
{
    private static bool s_RegisteredManagers = false;
    private static Dictionary<Type, IManager> s_Managers = new Dictionary<Type, IManager>();
    private static List<IManager> s_UsedManagers = new List<IManager>();

    protected T CreateManager<T>() where T: IManager
    {
        T manager = default(T);

        //this function can't be called after all the managers have been registered and created
        if (s_RegisteredManagers)
        {
            Logger.Log("Managers cannot be registered outside of the RegisterManagers() function!", Logger.MessageLevel.MESSAGE_LEVEL_WARNING_LEVEL_0);
            return manager;
        }
        else
        {
            Type managerType = typeof(T);
            if (s_Managers.ContainsKey(managerType))
            {
                manager = (T)s_Managers[managerType];
                s_Managers.Remove(managerType);
            }
            else
            {
                manager = Activator.CreateInstance<T>();
            }

            s_UsedManagers.Add(manager);
        }

        return manager;
    }

    public Scene()
    {
        s_RegisteredManagers = false;
        RegisterManagers();
        s_RegisteredManagers = true;

        //clear all unused managers
        foreach(var kvp in s_Managers)
        {
            kvp.Value.Dismiss();
        }
        s_Managers.Clear();

        //copy all used managers back to the managers dictionary
        for(int i = 0; i < s_UsedManagers.Count; ++i)
        {
            IManager manager = s_UsedManagers[i];
            s_Managers.Add(manager.GetType(), manager);
        }
        s_UsedManagers.Clear();
    }

    protected abstract void RegisterManagers();

    public abstract void UpdateLoading(float deltaTimeSec);
    public abstract void UpdateScene(float deltaTimeSec);
    public abstract bool IsReady();
    public abstract float GetLoadPercentage();
    public abstract void LoadScene(bool loadAsync);
    public abstract void StartScene();
    public abstract void Dispose();
}