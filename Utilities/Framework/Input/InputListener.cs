﻿public interface InputListener {

    bool TouchDown(InputPointerBase input); //return true if event has been handled
    bool TouchUp(InputPointerBase input); //return true if event has been handled


}