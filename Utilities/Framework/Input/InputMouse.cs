﻿using UnityEngine;

public class InputMouse : InputPointerBase {

    public InputMouse()
    {
        
    }

    public bool HasMoved()
    {
        return false;
    }

    public Vector3 Position
    {
        get { return Input.mousePosition; }
    }

}
