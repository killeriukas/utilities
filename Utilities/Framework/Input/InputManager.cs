﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class InputManager
{

    private static List<InputListener> s_InputListeners;

    static InputManager()
    {
        s_InputListeners = new List<InputListener>();
    }

    public static void AddListener(InputListener me)
    {
        s_InputListeners.Add(me);
    }

    public static void RemoveListener(InputListener me)
    {
        s_InputListeners.Remove(me);
    }

    public static void Update()
    {

        //mouse left button has been pressed
        if (Input.GetMouseButtonDown(0))
        {
            InputMouse mouse = new InputMouse();

            for (int i = 0; i < s_InputListeners.Count; ++i)
            {
                if (s_InputListeners[i].TouchDown(mouse))
                {
                    break;
                }
            }
            return;
        }

        //mouse left button has been released
        if (Input.GetMouseButtonUp(0))
        {
            InputMouse mouse = new InputMouse();
            
            for (int i = 0; i < s_InputListeners.Count; ++i)
            {
                if (s_InputListeners[i].TouchUp(mouse))
                {
                    break;
                }
            }
            return;
        }
        




    }

}