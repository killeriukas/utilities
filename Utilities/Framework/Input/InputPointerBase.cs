﻿using UnityEngine;

public interface InputPointerBase
{
    bool HasMoved();
    Vector3 Position
    {
        get;
    }

}
