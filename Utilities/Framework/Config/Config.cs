﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

[JsonObject(MemberSerialization.OptIn)]
public sealed class Config
{

    [JsonProperty(PropertyName="Config")]
    private Dictionary<string, IConfig> m_Configs = new Dictionary<string, IConfig>();
        
    public void Add(IConfig config)
    {
        if(config == null)
        {
            return;
        }

        string configType = config.GetType().Name;

        if(m_Configs.ContainsKey(configType))
        {
            return;
        }

        m_Configs.Add(configType, config);
    }

    public T GetConfig<T>() where T : IConfig
    {
        string name = typeof(T).Name;
        if(!m_Configs.ContainsKey(name))
        {
            return default(T);
        }

        T config = (T)m_Configs[name];

        return config;
    }

    public bool IsEmpty()
    {
        return m_Configs.Count == 0;
    }
}