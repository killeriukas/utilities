﻿using Newtonsoft.Json;

public sealed class ConfigController : Singleton<ConfigController>
{
    private const string DEFAULT_FILE_NAME = "Config.ini";

    private Config m_DefaultConfig = new Config();
    private Config m_Config;

    private ConfigController()
    {
        DeserializeConfigFile();
    }

    private void DeserializeConfigFile()
    {
        bool fileExists = System.IO.File.Exists(DEFAULT_FILE_NAME);

        if (fileExists)
        {
            string file = System.IO.File.ReadAllText(DEFAULT_FILE_NAME);
            Config compiledFile = JsonConvert.DeserializeObject<Config>(file, GetJsonNameAutoHandling());
            m_Config = compiledFile;
        }
    }

    private void SerializeDefaultConfigFile()
    {
        string serialized = JsonConvert.SerializeObject(m_DefaultConfig, Formatting.Indented, GetJsonNameAutoHandling());
        System.IO.File.WriteAllText(DEFAULT_FILE_NAME, serialized);
    }

    private JsonSerializerSettings GetJsonNameAutoHandling()
    {
        JsonSerializerSettings jss = new JsonSerializerSettings();
        jss.TypeNameHandling = TypeNameHandling.Auto;
        return jss;
    }

    public static T GetConfig<T>() where T: IConfig
    {
        //if config doesn't exist, create default config file
        if (ConfigController.Instance.m_Config == null)
        {
            if(ConfigController.Instance.m_DefaultConfig.IsEmpty())
            {
                throw new System.ArgumentNullException("Config file not found! Default config file is empty too!");
            }

            ConfigController.Instance.SerializeDefaultConfigFile();
            ConfigController.Instance.DeserializeConfigFile();
        }

         return ConfigController.Instance.m_Config.GetConfig<T>();
    }

    public static void AddConfig(IConfig config)
    {
        ConfigController.Instance.m_DefaultConfig.Add(config);
    }

    public override void Dispose()
    {
        
    }
}