﻿using System.Collections.Generic;
using UnityEngine;

public class UIPopupController
{
    private Stack<UIPopupVisual> m_UIPopupStack;
    private List<UIPopupVisual> m_UIPopupList;
    //private UIPanel m_UIPanel;

    //public void SetUIPanel (UIPanel uiPanel)
    //{
    //    m_UIPanel = uiPanel;
    //}

    public UIPopupController()
    {
		m_UIPopupList = new List<UIPopupVisual>();
		m_UIPopupStack = new Stack<UIPopupVisual>();
    }

	public T Create<T>(UIPopup popup) where T : UIPopupVisual
	{
		return (T)CreatePopup(typeof(T), popup);
	}

	protected virtual UIPopupVisual CreatePopup(System.Type type, UIPopup popup)
	{
		string popupPath = "Prefabs/Popups/" + type.ToString();
		UnityEngine.GameObject popupGO = (UnityEngine.GameObject)UnityEngine.GameObject.Instantiate(ResourceLoader.Load(popupPath));
//        popupGO.transform.parent = m_UIPanel.gameObject.transform;
		popupGO.transform.localScale = Vector3.one;
		popupGO.transform.localPosition = Vector3.zero;
		popupGO.SetActive(false);
		UIPopupVisual visual = popupGO.GetComponent<UIPopupVisual>();
		visual.Setup(popup);
		return visual;
	}

	public void AddPopup(UIPopupVisual popup, bool blocker)
    {
        if (blocker)
        {
			TryDeactivateBlockerPopup();
            m_UIPopupStack.Push(popup);
			TryActivateBlockerPopup();
        }
        else
        {
            m_UIPopupList.Add(popup);
			ActivatePopup(popup);
        }
    }

	private void DeactivatePopup(UIPopupVisual popupVisual)
	{
		popupVisual.PopupWillDisappear();
		popupVisual.gameObject.SetActive(false);
		popupVisual.PopupDidDisappear();
	}

	private void ActivatePopup(UIPopupVisual popupVisual)
	{
		popupVisual.PopupWillAppear();
		popupVisual.gameObject.SetActive(true);
		popupVisual.PopupDidAppear();
	}

	public void DismissPassedPopup(UIPopupVisual popupVisual)
	{
		m_UIPopupList.Remove(popupVisual);
		DeactivatePopup(popupVisual);
		popupVisual.Dispose();
	}

	//tries to activate blocker popup if any already exists
	private void TryActivateBlockerPopup()
	{
		if(m_UIPopupStack.Count > 0)
		{
			UIPopupVisual visual = m_UIPopupStack.Peek();
			visual.PopupWillAppear();
			visual.gameObject.SetActive(true);
			visual.PopupDidAppear();
		}
	}

	//tries to deactivate blocker popup if any already exists
	private void TryDeactivateBlockerPopup()
	{
		if(m_UIPopupStack.Count > 0)
		{
			UIPopupVisual visual = m_UIPopupStack.Peek();
			visual.PopupWillDisappear();
			visual.gameObject.SetActive(false);
			visual.PopupDidDisappear();
		}
	}

	//dismiss the last blocker popup
	public void DismissLastBlockerPopup()
	{
		TryDeactivateBlockerPopup();
		m_UIPopupStack.Pop().Dispose();
		TryActivateBlockerPopup();
	}

    public void Dispose()
    {
        while(m_UIPopupStack.Count > 0)
		{
			DismissLastBlockerPopup();
		}

		for(int i = 0; i < m_UIPopupList.Count; ++i)
		{
			DeactivatePopup(m_UIPopupList[i]);
			m_UIPopupList[i].Dispose();
		}
		m_UIPopupList.Clear();
    }
}