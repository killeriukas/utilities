﻿using UnityEngine;

public class UIScreenController
{
    private UIScreenVisual m_UIScreen;
    //private UIPanel m_UIPanel;
	
    //public void SetUIPanel (UIPanel uiPanel)
    //{
    //    m_UIPanel = uiPanel;
    //}

    public UIScreenController()
    {

    }

    public void SetScreen(UIScreenVisual screen)
    {
		if(m_UIScreen != null)
		{
			Dispose();
		}
        m_UIScreen = screen;
      //  m_UIScreen.SetMainPanel(m_UIPanel);
		m_UIScreen.ScreenWillAppear();
		m_UIScreen.gameObject.SetActive(true);
		m_UIScreen.ScreenDidAppear();
    }

    public UIScreenVisual GetScreen()
    {
        return m_UIScreen;
    }

    public void Dispose()
    {
		m_UIScreen.ScreenWillDisappear();
		m_UIScreen.gameObject.SetActive(false);
		m_UIScreen.ScreenDidDisappear();
		m_UIScreen.Dispose();
		m_UIScreen = null;
    }

    public T Create<T>(UIScreen screen) where T : UIScreenVisual
    {
        return (T)CreateScreen(typeof(T), screen);
    }

    protected virtual UIScreenVisual CreateScreen(System.Type type, UIScreen screen)
    {
        string screenPath = "Prefabs/Screens/" + type.ToString();
        UnityEngine.GameObject screenGO = (UnityEngine.GameObject)UnityEngine.GameObject.Instantiate(ResourceLoader.Load(screenPath));
		screenGO.transform.localScale = Vector3.one;
		screenGO.transform.localPosition = Vector3.zero;
		screenGO.SetActive(false);
		UIScreenVisual visual = screenGO.GetComponent<UIScreenVisual>();
		visual.Setup(screen);
		return visual;
    }
}