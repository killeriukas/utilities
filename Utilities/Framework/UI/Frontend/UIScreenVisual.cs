using UnityEngine;
using System.Collections;

public abstract class UIScreenVisual : CacheMonoBehaviour {

    //public enum ScreenPosition
    //{
    //    BOTTOM = 0,
    //    LEFT,
    //    TOP,
    //    RIGHT,
    //    CENTER
    //}

    private Canvas m_Canvas;

    private UIScreen m_UIScreen;
    //private UIPanel m_MainPanel;
 //   private PanelAnchors m_MainPanelAnchors;

	public abstract void ScreenWillAppear();
	public abstract void ScreenDidAppear();
	public abstract void ScreenWillDisappear();
	public abstract void ScreenDidDisappear();

//    protected void AttachToAnchor(Transform transform, ScreenPosition position)
//    {
//        switch(position)
//        {
//            case ScreenPosition.BOTTOM:
//  //              transform.parent = m_MainPanelAnchors.GetBottomTransform();
//                break;
//            case ScreenPosition.CENTER:
//  //              transform.parent = m_MainPanelAnchors.GetCenterTransform();
//                break;
//            case ScreenPosition.LEFT:
//  //              transform.parent = m_MainPanelAnchors.GetLeftTransform();
//                break;
//            case ScreenPosition.RIGHT:
////                transform.parent = m_MainPanelAnchors.GetRightTransform();
//                break;
//            case ScreenPosition.TOP:
//  //              transform.parent = m_MainPanelAnchors.GetTopTransform();
//                break;
//            default:
//                transform.parent = null;
//                break;
//        }

//        transform.localPosition = Vector3.zero;
//        transform.localScale = Vector3.one;
//    }

    protected virtual void Awake()
    {
        m_Canvas = gameObject.GetComponent<Canvas>();
        m_Canvas.worldCamera = GameObject.FindGameObjectWithTag("UICamera").GetComponent<Camera>();
    }

	public virtual void Dispose()
	{
		GameObject.Destroy(gameObject);
	}

    public void Setup(UIScreen screenInfo)
    {
        m_UIScreen = screenInfo;
    }

    protected T GetCore<T>() where T : UIScreen
    {
        return (T)m_UIScreen;
    }

 //   public void SetMainPanel(UIPanel uiPanel)
 //   {
 //       m_MainPanel = uiPanel;
 ////       m_MainPanelAnchors = m_MainPanel.GetComponent<PanelAnchors>();
 //   }
}
