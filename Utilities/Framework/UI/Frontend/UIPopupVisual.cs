﻿using UnityEngine;
using System.Collections;

public abstract class UIPopupVisual : MonoBehaviour {

	private UIPopup m_Popup;

	public void Setup(UIPopup popup)
	{
		m_Popup = popup;
	}



	public abstract void PopupWillAppear ();
	public abstract void PopupDidAppear ();
	public abstract void PopupWillDisappear ();
	public abstract void PopupDidDisappear ();

	public virtual void Dispose ()
	{
		GameObject.Destroy(gameObject);

		//screenGO.transform.parent = m_UICanvas;

	}
}
