﻿using UnityEngine;

//wrapper class for unity mathf library
public static class MathLib
{
    public static float Clamp01(float value)
    {
        return Mathf.Clamp01(value);
    }

    public static float Clamp(float value, float min, float max)
    {
        return Mathf.Clamp(value, min, max);
    }

    public static int Clamp(int value, int min, int max)
    {
        return Mathf.Clamp(value, min, max);
    }

}