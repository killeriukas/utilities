﻿public class LoggerFile : LoggerBase
{

    private System.IO.StreamWriter m_File;

    private string m_DefaultFolder = "Log";
    private string m_DefaultPrefix = ".log";

    public LoggerFile(string p_fileName = "")
    {
        string fileName = p_fileName;

        if (fileName.Equals(""))
        {
            fileName = "Error";
        }

        System.IO.Directory.CreateDirectory(m_DefaultFolder);
        m_File = new System.IO.StreamWriter(m_DefaultFolder + "\\" + fileName + m_DefaultPrefix, true);
    }

    public virtual void Log(string message)
    {
        m_File.WriteLine(message);
        m_File.Flush();
    }

    public virtual void DestroyLogger()
    {
        if(m_File != null)
        {
            m_File.Close();
            m_File = null;
        }
    }

}

