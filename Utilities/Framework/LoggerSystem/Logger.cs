﻿public sealed class Logger {

	public enum LogTo {
		LOG_TO_UNITY = 0,
		LOG_TO_FILE
	}

    public enum MessageLevel
    {
        MESSAGE_LEVEL_INFO_LEVEL_0 = 0, //just information
        MESSAGE_LEVEL_INFO_LEVEL_1,
        MESSAGE_LEVEL_INFO_LEVEL_2,
        MESSAGE_LEVEL_INFO_LEVEL_3,
        MESSAGE_LEVEL_INFO_LEVEL_4,     //important information
        MESSAGE_LEVEL_WARNING_LEVEL_0,
        MESSAGE_LEVEL_WARNING_LEVEL_1,
        MESSAGE_LEVEL_WARNING_LEVEL_2,
        MESSAGE_LEVEL_WARNING_LEVEL_3,
        MESSAGE_LEVEL_WARNING_LEVEL_4,
        MESSAGE_LEVEL_ERROR_LEVEL_0,    //error - warning type
        MESSAGE_LEVEL_ERROR_LEVEL_1,
        MESSAGE_LEVEL_ERROR_LEVEL_2,
        MESSAGE_LEVEL_ERROR_LEVEL_3,
        MESSAGE_LEVEL_ERROR_LEVEL_4     //fatal error - crash
    };

	private static Logger s_Logger;
    private LoggerBase m_Logger;
    private MessageLevel m_minMessageLevel = MessageLevel.MESSAGE_LEVEL_INFO_LEVEL_0;
    private MessageLevel m_maxMessageLevel = MessageLevel.MESSAGE_LEVEL_ERROR_LEVEL_4;

    //public static void Log(string message, MessageLevel level)
    //{
    //    Logger.Instance.LogInfo(message, level);
    //}

    public static void Log(object message)
    {
        if (message != null)
        {
            Logger.Instance.LogInfo(message.ToString(), MessageLevel.MESSAGE_LEVEL_INFO_LEVEL_0);
        }
    }

    public static void Log(object message, MessageLevel level)
    {
        if(message != null)
        {
            Logger.Instance.LogInfo(message.ToString(), level);
        }
    }

    //public static void Log(string message)
    //{
    //    Logger.Instance.LogInfo(message, MessageLevel.MESSAGE_LEVEL_INFO_LEVEL_0);
    //}

	public static Logger Instance
	{
		get {
            return s_Logger == null ? s_Logger = new Logger() : s_Logger;
		}
		private set{}
	}

    public void SetLogTo(LogTo logTo)
    {
        switch (logTo)
        {
            case LogTo.LOG_TO_UNITY:
                //m_Logger = new LoggerUnity();
                break;
            case LogTo.LOG_TO_FILE:
                m_Logger = new LoggerFile();
                m_minMessageLevel = MessageLevel.MESSAGE_LEVEL_INFO_LEVEL_0;
                m_maxMessageLevel = MessageLevel.MESSAGE_LEVEL_ERROR_LEVEL_4;
                break;
        }
    }

    public void ChangeMinMessageLevel(MessageLevel level)
    {
        m_minMessageLevel = level;
    }

    public void ChangeMaxMessageLevel(MessageLevel level)
    {
        m_maxMessageLevel = level;
    }

	private Logger()
    {
        SetLogTo(LogTo.LOG_TO_FILE);
    }

    private string GetCurrentTime()
    {
        return System.DateTime.UtcNow.ToShortDateString() + " " + System.DateTime.UtcNow.ToLongTimeString();
    }

    private void LogInfo(string message, MessageLevel level)
    {

        if (m_minMessageLevel > level || level > m_maxMessageLevel)
        {
            LogWarning(message + " [Message level wasn't high enough!] MIN LVL: " + m_minMessageLevel + " MAX LVL: " + m_maxMessageLevel + " CUR LVL: " + level);
            return;
        }

        switch(level)
        {
            case MessageLevel.MESSAGE_LEVEL_INFO_LEVEL_0:
            case MessageLevel.MESSAGE_LEVEL_INFO_LEVEL_1:
            case MessageLevel.MESSAGE_LEVEL_INFO_LEVEL_2:
            case MessageLevel.MESSAGE_LEVEL_INFO_LEVEL_3:
            case MessageLevel.MESSAGE_LEVEL_INFO_LEVEL_4:
                LogMessage(message);
                break;
            case MessageLevel.MESSAGE_LEVEL_WARNING_LEVEL_0:
            case MessageLevel.MESSAGE_LEVEL_WARNING_LEVEL_1:
            case MessageLevel.MESSAGE_LEVEL_WARNING_LEVEL_2:
            case MessageLevel.MESSAGE_LEVEL_WARNING_LEVEL_3:
            case MessageLevel.MESSAGE_LEVEL_WARNING_LEVEL_4:
                LogWarning(message);
                break;
            case MessageLevel.MESSAGE_LEVEL_ERROR_LEVEL_0:
            case MessageLevel.MESSAGE_LEVEL_ERROR_LEVEL_1:
            case MessageLevel.MESSAGE_LEVEL_ERROR_LEVEL_2:
            case MessageLevel.MESSAGE_LEVEL_ERROR_LEVEL_3:
            case MessageLevel.MESSAGE_LEVEL_ERROR_LEVEL_4:
                LogError(message);
                break;
            default:
                LogWarning(message + " [Message level was not recognized!]");
                break;
        }
    }

    public void DestroyLogger()
    {
        if (m_Logger != null)
        {
            m_Logger.DestroyLogger();
            m_Logger = null;
        }
        s_Logger = null;
    }

    private void LogWarning(string message)
    {
        m_Logger.Log("[" + GetCurrentTime() + "] WARNING: " + message);
    }

    private void LogError(string message)
    {
        m_Logger.Log("[" + GetCurrentTime() + "] ERROR: " + message + System.Environment.NewLine + System.Environment.StackTrace);
    }

    private void LogMessage(string message)
    {
        m_Logger.Log("[" + GetCurrentTime() + "] INFO: " + message);
    }
}
