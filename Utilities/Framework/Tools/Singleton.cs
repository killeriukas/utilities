﻿using System;

public abstract class Singleton<T>
{
    private static T s_Singleton = (T)Activator.CreateInstance(typeof(T), true);
    static Singleton() { }
    protected Singleton(){ }
    public static T Instance { get { return s_Singleton; }}
    public abstract void Dispose();
}
