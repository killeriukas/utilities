﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

public class DllLoader
{

    public const string DEFAULT_LIBRARY_DIR = "lib";

    //unblocking files, which would be otherwise blocked through the unmanaged code (cannot work with blocked files)
    [DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool DeleteFile(string name);

    private static bool UnblockFile(string fileName)
    {
        return DeleteFile(fileName + ":Zone.Identifier");
    }

    private DirectoryInfo m_RootDirectory;

    public DllLoader(string dllFilesRootFolder)
    {
        Functions.Directory.EnsureExists(dllFilesRootFolder, false);

        m_RootDirectory = new DirectoryInfo(dllFilesRootFolder);

        AppDomain.CurrentDomain.AssemblyResolve += DllFolderForward;
    }

    private Assembly DllFolderForward(object sender, ResolveEventArgs args)
    {
        int dllNameEndIndex = args.Name.IndexOf(',');

        string fileName = args.Name;
        if(dllNameEndIndex > 0)
        {
            string fileNameNoExtension = args.Name.Substring(0, dllNameEndIndex);
            fileName = fileNameNoExtension + ".dll";
        }

        return FindAssembly(m_RootDirectory, fileName);
    }

    private Assembly FindAssembly(DirectoryInfo curDirectory, string dllName)
    {
        //get all files in the directory first
        FileInfo[] allRootFiles = curDirectory.GetFiles("*.dll");

        //search for the looking dll
        foreach (FileInfo file in allRootFiles)
        {
            //assembly has been found
            if (file.Name.Equals(dllName))
            {
                bool unlocked = UnblockFile(file.FullName);
                Assembly loadedAssembly = Assembly.LoadFrom(file.FullName);
                return loadedAssembly;
            }
        }

        //if dll was not found, get all directories and do this again
        DirectoryInfo[] allRootDirectories = curDirectory.GetDirectories();

        foreach (DirectoryInfo directory in allRootDirectories)
        {
            Assembly foundAssembly = FindAssembly(directory, dllName);
            if (foundAssembly != null)
            {
                return foundAssembly;
            }
        }

        return null;
    }

}