﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Functions
{
    public static class TimeConstants
    {
        public static readonly int MILISECONDS_IN_SECOND = 1000;
        public static readonly int SECONDS_IN_MINUTE = 60;
        public static readonly int MINUTES_IN_HOUR = 60;
        public static readonly int HOURS_IN_DAY = 24;

        public static readonly int DAYS_IN_MONTH = 30;
        public static readonly int DAYS_IN_YEAR = 365;

        public static readonly int HOURS_IN_YEAR = DAYS_IN_YEAR * 24;
        public static readonly int SECONDS_IN_HOUR = MINUTES_IN_HOUR * SECONDS_IN_MINUTE;
        public static readonly int SECONDS_IN_DAY = SECONDS_IN_HOUR * HOURS_IN_DAY;
        public static readonly int SECONDS_IN_YEAR = SECONDS_IN_DAY * DAYS_IN_YEAR;
    }

    public static class Money
    {
        public enum Currency
        {
            GBP,
            USD,
            EUR
        };

        private static Dictionary<Currency, string> s_CurrencySign = new Dictionary<Currency, string>();

        static Money()
        {
            s_CurrencySign.Add(Currency.GBP, "£");
            s_CurrencySign.Add(Currency.USD, "$");
            s_CurrencySign.Add(Currency.EUR, "€");
        }

        public static string GetSign(Currency currency)
        {
            string sign = "no sign found";
            if(s_CurrencySign.ContainsKey(currency))
            {
                sign = s_CurrencySign[currency];
            }
            return sign;
        }

    }

    public static class Directory
    {
        public static void EnsureExists(string path, bool forceClean)
        {
            if(System.IO.Directory.Exists(path))
            {
                if(forceClean)
                {
                    System.IO.Directory.Delete(path, true);
                    System.IO.Directory.CreateDirectory(path);
                }
            }
            else
            {
                System.IO.Directory.CreateDirectory(path);
            }
        }

        public static void EnsureDelete(string path)
        {
            if(System.IO.Directory.Exists(path))
            {
                System.IO.Directory.Delete(path, true);
            }
        }
    }

    public static class Convert
    {

        public static List<T> OneToList<T>(T one)
        {
            List<T> temp = new List<T>();
            temp.Add(one);
            return temp;
        }

        public static string FirstCapital(string word, bool minimizeLatter = true)
        {
            //TODO: test it with 0 characters
            if(string.IsNullOrEmpty(word))
            {
                throw new System.ArgumentNullException();
            }
            string latter = word.Substring(1);
            return char.ToUpper(word[0]) + (minimizeLatter ? latter.ToLower() : latter);
        }

        public static string MoneyHumanReadable(Money.Currency currency, float amount, string formatter = "n2")
        {
            return Money.GetSign(currency) + amount.ToString(formatter);
        }

        public static class Time
        {
            public static int SecondsToYears(int seconds)
            {
                return seconds / TimeConstants.SECONDS_IN_YEAR;
            }

            public static int SecondsToDays(int seconds)
            {
                return seconds / TimeConstants.SECONDS_IN_DAY;
            }

            public static string SecondsToYearsDays(int seconds)
            {
              //  TimeSpan time = TimeSpan.FromSeconds(seconds);
                string readable = "{0}y. {1}d.";
                int years = SecondsToYears(seconds);
                int days = SecondsToDays(seconds - years * TimeConstants.SECONDS_IN_YEAR);
                return string.Format(readable, years, days);
            }

        }


    }

    public static class UnityHelper
    { 
        
       //private static IEnumerator DoAc

        public static IEnumerator LerpNumber(float startDelay, float animationTime, float startNumber, float endNumber, Action<float> curValueCallback)
        {
            //skip delay, if it's zero seconds, because yielding skips the frame
            if(!Mathf.Approximately(startDelay, 0.0f))
            {
                float curDelayTime = 0.0f;
                while(IsWaiting(ref curDelayTime, startDelay))
                {
                    yield return null;
                }
            }

            //skip when values are the same
            if(Mathf.Approximately(startNumber, endNumber))
            {
                yield break;
            }

            float startTime = 0.0f;
            while(startTime < animationTime)
            {
                startTime += Time.smoothDeltaTime;
                float perc = Functions.Math.GetPercentage(startTime, animationTime);
                float curValue = Mathf.Lerp(startNumber, endNumber, perc);
                curValueCallback(curValue);
                yield return null;
            }
            curValueCallback(endNumber);
        }

        public static bool IsWaiting(ref float curTime, float duration)
        {
            curTime += Time.deltaTime;
            return curTime < duration;
        }

    }

    public static class Math
    {

        public static float GetPercentage(float curTime, float duration)
        {
            return Mathf.Clamp01(curTime / duration);
        }

        public static float GetInversePercentage(float curTime, float duration)
        {
            return 1.0f - GetPercentage(curTime, duration);
        }

    }

}