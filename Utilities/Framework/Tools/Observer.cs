﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public sealed class Observer<Data, EventType>
    where Data : class
    //where EventType : Enum
{

    private List<Action<Data, EventType>> m_Observers = new List<Action<Data, EventType>>();

    public void AddObserver(Action<Data, EventType> listener)
    {
        m_Observers.Add(listener);
    }

    public void RemoveObserver(Action<Data, EventType> listener)
    {
        m_Observers.Remove(listener);
    }

    public void Notify(Data data, EventType eventType)
    {
        foreach(Action<Data, EventType> action in m_Observers)
        {
            action(data, eventType);
        }
    }

}
