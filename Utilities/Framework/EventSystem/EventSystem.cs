﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public static class EventSystem {

    //public enum EventType
    //{
    //    EVENT_TYPE_CLICK = 0,
    //    EVENT_TYPE_DOUBLE_CLICK
    //    //etc
    //}

    private sealed class EventToFuncName
    {
        public EventListener listener;
        public Action<EventBase> event_delegate;
        public static EventToFuncName CreateAttachment(EventListener listener, Action<EventBase> funcName)
        {
            EventToFuncName newAttachment = new EventToFuncName();
            newAttachment.listener = listener;
            newAttachment.event_delegate = funcName;
            return newAttachment;
        }
    }

    private static Dictionary<string, List<EventToFuncName>> s_EventDic = new Dictionary<string, List<EventToFuncName>>();
    //private static Dictionary<EventListener, Action<EventBase>> s_EventToAction = 
    //private static void SetActionByType(EventType eventType, Action<EventBase> functionName, EventListener listener)
    //{
    //    switch(eventType)
    //    {
    //        case EventType.EVENT_TYPE_CLICK:
    //            //listener.event_delegate = OnClick;
    //            break;
    //        case EventType.EVENT_TYPE_DOUBLE_CLICK:
    //            return "OnDoubleClick";
    //      //  default:
    //        //    Logger.Log("Listener got unknown event type!", Logger.MessageLevel.MESSAGE_LEVEL_ERROR_LEVEL_0);
    //    }
    //    Logger.Log("Listener got unknown event type!", Logger.MessageLevel.MESSAGE_LEVEL_ERROR_LEVEL_0);
    //    return "";
    //}

    

    //public static void AddListener(EventType eventType, EventListener listener)
    //{
    //   // AddListener(GetListenerFunctionName(eventType), GetListenerFunctionName(eventType), listener);
    //}

    public static void RemoveListener(string eventType, EventListener listener)
    {
        if (s_EventDic.ContainsKey(eventType))
        {
            List<EventToFuncName> foundList = s_EventDic[eventType];
            int listLength = foundList.Count;
            for (int i = 0; i < listLength; ++i)
            {
                if(foundList[i].listener == listener)
                {
                    foundList.RemoveAt(i);
                    break;
                }
            }
            if(0 == foundList.Count)
            {
                s_EventDic.Remove(eventType);
            }
        }
        else
        {
            Logger.Log("Tried to remove non-existent listener", Logger.MessageLevel.MESSAGE_LEVEL_WARNING_LEVEL_0);
        }
    }

    public static void AddListener(string eventType, Action<EventBase> functionName, EventListener listener)
    {
        //listener.event_delegate = functionName;
        if (s_EventDic.ContainsKey(eventType))
        {
            s_EventDic[eventType].Add(EventToFuncName.CreateAttachment(listener, functionName));
        }
        else
        {
            List<EventToFuncName> newListeners = new List<EventToFuncName>();
            newListeners.Add(EventToFuncName.CreateAttachment(listener, functionName));
            s_EventDic.Add(eventType, newListeners);
        }
    }

    public static void TriggerEvent(string actionName, EventBase eventInfo)
    {
        if (s_EventDic.ContainsKey(actionName))
        {
            List<EventToFuncName> foundList = s_EventDic[actionName];
            int listLength = foundList.Count;
            for (int i = 0; i < listLength; ++i)
            {
                foundList[i].event_delegate(eventInfo);
            }
        }
        else
        {
            Logger.Log("Tried to trigger non-existent event", Logger.MessageLevel.MESSAGE_LEVEL_WARNING_LEVEL_0);
        }
    }
}

/*
using System;
using System.Collections.Generic;
public sealed class EventSystem {

    //public enum EventType
    //{
    //    EVENT_TYPE_CLICK = 0,
    //    EVENT_TYPE_DOUBLE_CLICK
    //    //etc
    //}

    private sealed class EventToFuncName
    {
        public EventListener listener;
        public Action<EventBase> event_delegate;
        public static EventToFuncName CreateAttachment(EventListener listener, Action<EventBase> funcName)
        {
            EventToFuncName newAttachment = new EventToFuncName();
            newAttachment.listener = listener;
            newAttachment.event_delegate = funcName;
            return newAttachment;
        }
    }

    private static Dictionary<string, List<EventToFuncName>> s_EventDic = new Dictionary<string, List<EventToFuncName>>();
    //private static Dictionary<EventListener, Action<EventBase>> s_EventToAction = 
    //private static void SetActionByType(EventType eventType, Action<EventBase> functionName, EventListener listener)
    //{
    //    switch(eventType)
    //    {
    //        case EventType.EVENT_TYPE_CLICK:
    //            //listener.event_delegate = OnClick;
    //            break;
    //        case EventType.EVENT_TYPE_DOUBLE_CLICK:
    //            return "OnDoubleClick";
    //      //  default:
    //        //    Logger.Log("Listener got unknown event type!", Logger.MessageLevel.MESSAGE_LEVEL_ERROR_LEVEL_0);
    //    }
    //    Logger.Log("Listener got unknown event type!", Logger.MessageLevel.MESSAGE_LEVEL_ERROR_LEVEL_0);
    //    return "";
    //}

    

    //public static void AddListener(EventType eventType, EventListener listener)
    //{
    //   // AddListener(GetListenerFunctionName(eventType), GetListenerFunctionName(eventType), listener);
    //}

    public static void RemoveListener(string eventType, EventListener listener)
    {
        if (s_EventDic.ContainsKey(eventType))
        {
            List<EventToFuncName> foundList = s_EventDic[eventType];
            int listLength = foundList.Count;
            for (int i = 0; i < listLength; ++i)
            {
                if(foundList[i].listener == listener)
                {
                    foundList.RemoveAt(i);
                    break;
                }
            }
            if(0 == foundList.Count)
            {
                s_EventDic.Remove(eventType);
            }
        }
        else
        {
            Logger.Log("Tried to remove non-existent listener", Logger.MessageLevel.MESSAGE_LEVEL_WARNING_LEVEL_0);
        }
    }

    //THIS ONE WOULD BE MUCH MORE EXPENSIVE
    //public static void RemoveListener(EventListener listener)
    //{
    //    foreach(var kvp in s_EventDic)
    //    {
    //        List<EventToFuncName> eventList = kvp.Value;
    //        for (int i = eventList.Count - 1; i >= 0; --i)
    //        {
    //            if(eventList[i].listener == listener)
    //            {
    //                eventList.RemoveAt(i);
    //            }
    //        }
    //        if()
    //        {

    //        }
    //    }
    //}

    public static void AddListener(string eventType, Action<EventBase> functionName, EventListener listener)
    {
        //listener.event_delegate = functionName;
        if (s_EventDic.ContainsKey(eventType))
        {
            s_EventDic[eventType].Add(EventToFuncName.CreateAttachment(listener, functionName));
        }
        else
        {
            List<EventToFuncName> newListeners = new List<EventToFuncName>();
            newListeners.Add(EventToFuncName.CreateAttachment(listener, functionName));
            s_EventDic.Add(eventType, newListeners);
        }
    }

    public static void TriggerEvent(string actionName, EventBase eventInfo)
    {
        if (s_EventDic.ContainsKey(actionName))
        {
            List<EventToFuncName> foundList = s_EventDic[actionName];
            int listLength = foundList.Count;
            for (int i = 0; i < listLength; ++i)
            {
                foundList[i].event_delegate(eventInfo);
            }
        }
        else
        {
            Logger.Log("Tried to trigger non-existent event", Logger.MessageLevel.MESSAGE_LEVEL_WARNING_LEVEL_0);
        }
    }
}


*/
